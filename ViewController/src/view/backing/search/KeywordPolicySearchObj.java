package view.backing.search;

import java.util.Date;

public class KeywordPolicySearchObj {
    public KeywordPolicySearchObj() {
        super();
    }

    private String vdkvgwkey;
    private String strTitle;
    private String dynamicSummary;
    private String summary;
    private String K2_itemId;
    private String K2_GuidelineTitle;
    private String K2_Summary;
    private String K2_LastUpdated;
    private String K2_keywords;
    private String K2_NewIssuanceDate;
    private String K2_Highlights;
    private String K2_partitionTypeId;
    private String K2_Applicability;
    private String K2_IssuingDepartment;
    private String fileId;

    private Date K2_IssuanceDateFormatted;
    private String[] K2_KeywordsArray;
    private int arrayLength;


    public void setVdkvgwkey(String vdkvgwkey) {
        this.vdkvgwkey = vdkvgwkey;
    }

    public String getVdkvgwkey() {
        return vdkvgwkey;
    }

    public void setStrTitle(String strTitle) {
        this.strTitle = strTitle;
    }

    public String getStrTitle() {
        return strTitle;
    }

    public void setDynamicSummary(String dynamicSummary) {
        this.dynamicSummary = dynamicSummary;
    }

    public String getDynamicSummary() {
        return dynamicSummary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getSummary() {
        return summary;
    }

    public void setK2_itemId(String K2_itemId) {
        this.K2_itemId = K2_itemId;
    }

    public String getK2_itemId() {
        return K2_itemId;
    }

    public void setK2_GuidelineTitle(String K2_GuidelineTitle) {
        this.K2_GuidelineTitle = K2_GuidelineTitle;
    }

    public String getK2_GuidelineTitle() {
        return K2_GuidelineTitle;
    }

    public void setK2_Summary(String K2_Summary) {
        this.K2_Summary = K2_Summary;
    }

    public String getK2_Summary() {
        return K2_Summary;
    }

    public void setK2_LastUpdated(String K2_LastUpdated) {
        this.K2_LastUpdated = K2_LastUpdated;
    }

    public String getK2_LastUpdated() {
        return K2_LastUpdated;
    }

    public void setK2_keywords(String K2_keywords) {
        this.K2_keywords = K2_keywords;
    }

    public String getK2_keywords() {
        return K2_keywords;
    }

    public void setK2_NewIssuanceDate(String K2_NewIssuanceDate) {
        this.K2_NewIssuanceDate = K2_NewIssuanceDate;
    }

    public String getK2_NewIssuanceDate() {
        return K2_NewIssuanceDate;
    }

    public void setK2_Highlights(String K2_Highlights) {
        this.K2_Highlights = K2_Highlights;
    }

    public String getK2_Highlights() {
        return K2_Highlights;
    }

    public void setK2_partitionTypeId(String K2_partitionTypeId) {
        this.K2_partitionTypeId = K2_partitionTypeId;
    }

    public String getK2_partitionTypeId() {
        return K2_partitionTypeId;
    }

    public void setK2_Applicability(String K2_Applicability) {
        this.K2_Applicability = K2_Applicability;
    }

    public String getK2_Applicability() {
        return K2_Applicability;
    }

    public void setK2_IssuingDepartment(String K2_IssuingDepartment) {
        this.K2_IssuingDepartment = K2_IssuingDepartment;
    }

    public String getK2_IssuingDepartment() {
        return K2_IssuingDepartment;
    }

    public void setK2_IssuanceDateFormatted(Date K2_IssuanceDateFormatted) {
        this.K2_IssuanceDateFormatted = K2_IssuanceDateFormatted;
    }

    public Date getK2_IssuanceDateFormatted() {
        return K2_IssuanceDateFormatted;
    }

    public void setK2_KeywordsArray(String[] K2_KeywordsArray) {
        this.K2_KeywordsArray = K2_KeywordsArray;
    }

    public String[] getK2_KeywordsArray() {
        return K2_KeywordsArray;
    }

    public void setArrayLength(int arrayLength) {
        this.arrayLength = arrayLength;
    }

    public int getArrayLength() {
        return arrayLength;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getFileId() {
        return fileId;
    }
}
